## Rust Lambda Function with Logging and Tracing
This project builds upon Mini Project 5 (Student Attendance Tracking) by implementing logging, integrating AWS X-Ray tracing, and connecting logs/traces to CloudWatch. This enables users to identify and address performance issues within the application, understand the calling relationships between various services, and analyze the path of requests flowing through the system.

### Implementation
The code utilizes the `tracing` and `tracing_subscriber` crates to configure the log subscriber in a Rust application. Specifically:

1. Employ `tracing_subscriber::FmtSubscriber::builder()` to create an `FmtSubscriber`. This subscriber formats logs and outputs them to the standard output (`stdout`).
2. Utilize `.with_max_level(Level::INFO)` to set the log level to a maximum of `INFO`. Only log messages of `INFO` level and above will be output. Messages with levels lower than `INFO`, such as `DEBUG` or `TRACE`, will be ignored.
3. Call `.finish()` to complete and return the configured FmtSubscriber.
4. Utilize `tracing::subscriber::set_global_default(subscriber)` to set the configured log subscriber as the global default subscriber. This means that subsequent `tracing` logs will use this subscriber for processing.

This configuration and setting of the global default `tracing` log subscriber enable the program to output formatted log messages at runtime, displaying only messages of `INFO` level and above.

Afterward, execute `cargo lambda build` and `cargo lambda deploy` to deploy the function to AWS Lambda. Additionally, create a new IAM role with the following six permissions: `IAMFullAccess`, `AWSXrayFullAccess`, `AWSXRayDaemonWriteAccess`, `AWSLambdaBasicExecutionRole`, `AWSLambda_FullAccess`, and `AmazonDynamoDBFullAccess`. This role allows reading and writing to DynamoDB and enables X-Ray tracing for the function.

Use the following JSON form to call the lambda function.
```JSON
{
  "id": "value1",
  "name": "value2",
  "course_id": 123
}
```

### Screenshots
The deployed Rust Lambda Function with logs and X-Ray tracing enabled:

![Screenshot 2024-03-06 at 6.41.47 PM.png](screenshot%2FScreenshot%202024-03-06%20at%206.41.47%20PM.png)

Rust Function Log streams:

![Screenshot 2024-03-06 at 6.41.10 PM.png](screenshot%2FScreenshot%202024-03-06%20at%206.41.10%20PM.png)

CloudWatch X-Ray traces:

![Screenshot 2024-03-06 at 6.38.36 PM.png](screenshot%2FScreenshot%202024-03-06%20at%206.38.36%20PM.png)

Trace details and logs:

![Screenshot 2024-03-06 at 6.40.32 PM.png](screenshot%2FScreenshot%202024-03-06%20at%206.40.32%20PM.png)