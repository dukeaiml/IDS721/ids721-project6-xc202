use aws_sdk_dynamodb::{Client, model::AttributeValue};
use aws_config::{load_from_env};
use chrono::prelude::*;
use lambda_runtime::{service_fn, Error, LambdaEvent};
use serde::{Deserialize, Serialize};
use serde_json::{Value};
use tracing::{Level};

#[derive(Debug, Clone, Serialize, Deserialize)]
pub struct Request {
    pub id: String,
    pub name: String,
    pub course_id: i32,
}

#[tokio::main]
async fn main() -> Result<(), Error> {
    let subscriber = tracing_subscriber::FmtSubscriber::builder()
        .with_max_level(Level::INFO)
        .finish();
    tracing::subscriber::set_global_default(subscriber).expect("Fail to set default subscriber");
    let func = service_fn(lambda_handler);
    lambda_runtime::run(func).await?;
    Ok(())
}

#[derive(Serialize)]
struct LambdaResponse {
    message: String,
}

async fn lambda_handler(event: LambdaEvent<Value>) -> Result<LambdaResponse, Error> {
    let request: Request = serde_json::from_value(event.payload)?;
    let count = record_attendance(request).await?;

    Ok(LambdaResponse {
        message: ("Attendance recorded successfully! Course attendance count: ").to_string() + &*count.unwrap().to_string(),
    })
}

// record attendance for a student in a course
async fn record_attendance(request: Request) -> Result<Result<i32, Error>, Error> {
    // Load AWS configuration from environment variables
    let config = load_from_env().await;
    let client = Client::new(&config);
    let table_name = "attendance_records";
    let student_id = request.id.clone();
    let time = Utc::now().to_string();

    // Prepare attribute values for DynamoDB put_item operation
    let id = AttributeValue::S(request.id);
    let name = AttributeValue::S(request.name);
    let course_id = AttributeValue::S(request.course_id.to_string());
    let timestamp = AttributeValue::S(time.clone());
    let student_attendance = AttributeValue::S(student_id.clone() + ":" + &*time);

    client.put_item().table_name(table_name).item("student_attendance", student_attendance).item("id", id).item("name", name).item("course_id", course_id).item("timestamp", timestamp).send().await?;

    // Count the number of course attendance records for the student
    let count = count_course(&client, table_name, student_id.clone(), request.course_id.to_string())
        .await
        .map_err(|err|{
            tracing::error!("Error counting course attendance: {}", err);
            Error::from(err.to_string())
        });
    Ok(count)
}

// count number of course attendance records for a student
pub async fn count_course(
    client: &Client,
    table_name: &str,
    id: String,
    course_id: String,
) -> Result<i32, Error> {
    // Execute scan operation to find course attendance records for the student
    let results = client
        .scan()
        .table_name(table_name)
        .filter_expression("#id = :id_val AND #course_id = :course_id_val")
        .expression_attribute_names("#id", "id")
        .expression_attribute_names("#course_id", "course_id")
        .expression_attribute_values(":id_val", AttributeValue::S(id))
        .expression_attribute_values(":course_id_val", AttributeValue::S(course_id))
        .send()
        .await
        .map_err(
            |err| {
                tracing::error!("Error scanning table: {}", err);
                Error::from(err.to_string())
            }
        );

    let mut count = -1;
    match results {
        Ok(scan_output) => {
            if let Some(items) = scan_output.items {
                count = items.len() as i32;
                Ok::<i32, Error>(count)
            } else {
                Ok(-1)
            }
        }
        Err(err) => {
            eprintln!("Error: {:?}", err);
            Ok(-1)
        }
    }.expect("No Match Results Found");
    Ok(count)
}


